package tk.maskedredstonerproz.libitumplayer.feature_music.domain.use_case

/**
 * An array of use cases for easy access
 * @param deleteSong The [DeleteSong] use case instance
 * @param addSong The [AddSong] use case instance
 * @author MaskedRedstonerProZ
 */
data class SongUseCases(
    val deleteSong: DeleteSong,
    val addSong: AddSong,
)
