package tk.maskedredstonerproz.libitumplayer.feature_music.presentation

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.DrawerValue
import androidx.compose.material.rememberDrawerState
import androidx.navigation.compose.rememberNavController
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.util.Navigation
import tk.maskedredstonerproz.libitumplayer.ui.theme.LibitumPlayerTheme

/**
 * Main activity class
 * @author MaskedRedstonerProZ
 */
class MainActivity : ComponentActivity() {

    /**
     * First function in activity lifecycle, called when the
     * activity instance is created
     * @param savedInstanceState The saved activity instance data
     * @author MaskedRedstonerProZ
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Set the composable content of the activity
        setContent {
            // Apply the theme
            LibitumPlayerTheme {
                // Get navigation controller
                val navController = rememberNavController()

                // Get drawer state
                val drawerState = rememberDrawerState(DrawerValue.Closed)

                // Call the Navigation composable, and set it's navigation controller, and drawerState
                Navigation(navController = navController, drawerState = drawerState)
            }
        }
    }
}