package tk.maskedredstonerproz.libitumplayer.feature_music.domain.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.lang.Exception

/**
 * The class that describes each [Song] object
 * @param name The name of the [Song]
 * @param artist The name of the [Song]'s artist
 * @param storagePath The path of the [Song] file on the filesystem
 * @param id The id of the [Song] in the database
 * @author MaskedRedstonerProZ
 */
@Entity
data class Song(

    @ColumnInfo
    val name: String,

    @ColumnInfo
    val artist: String,

    @ColumnInfo
    val storagePath: String,

    @PrimaryKey(autoGenerate = true)
    val id: Int = 0
)

/**
 * Thrown to indicate that the passed [Song] info is invalid
 * @param message The message that indicates why the [Song] info is invalid
 * @author MaskedRedstonerProZ
 */
class InvalidSongException(message: String): Exception(message)