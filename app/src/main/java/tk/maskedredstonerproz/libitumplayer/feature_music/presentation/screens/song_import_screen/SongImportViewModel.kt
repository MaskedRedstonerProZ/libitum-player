package tk.maskedredstonerproz.libitumplayer.feature_music.presentation.screens.song_import_screen

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import tk.maskedredstonerproz.libitumplayer.R
import tk.maskedredstonerproz.libitumplayer.feature_music.domain.model.Song
import tk.maskedredstonerproz.libitumplayer.feature_music.domain.model.SongInfoState
import tk.maskedredstonerproz.libitumplayer.feature_music.domain.use_case.SongUseCases
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.util.Event
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.util.Event.*
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.util.UiText
import java.io.File

/**
 * [SongImportViewModel] class for managing the states,
 * and general ui interaction
 * @param songUseCases The [SongUseCases] instance for accessing the database
 * @author MaskedRedstonerProZ
 */
class SongImportViewModel(
    private val songUseCases: SongUseCases
) : ViewModel() {

    /**
     * The counter for the imported song files,
     * allows setting info for multiple song files at a time
     * @author MaskedRedstonerProZ
     */
    var counter = 0

    /**
     * The state that houses all the imported songs
     * @author MaskedRedstonerProZ
     */
    private val _importedSongsState = mutableStateListOf<File>()
    val importedSongsState: SnapshotStateList<File> = _importedSongsState

    /**
     * The state that houses the song info
     * @author MaskedRedstonerProZ
     */
    private val _songInfoState = mutableStateOf(SongInfoState())
    val songInfoState by _songInfoState

    /**
     * Handles events sent to the [SongImportViewModel] from the [SongImportScreen]
     * @param event The event to handle
     * @author MaskedRedstonerProZ
     */
    fun onEvent(event: Event) = event.apply {

        when (this) {

            is UpdateImportedFileName -> {
                if (useInitialValue) {
                    _songInfoState.value = _songInfoState.value.copy(
                        importedFileName = UiText.StringResource(R.string.file_name_hint)
                    )
                    return@apply
                }

                try {
                    _songInfoState.value = _songInfoState.value.copy(
                        importedFileName = UiText.DynamicString(_importedSongsState[counter].nameWithoutExtension)
                    )
                } catch (e: IndexOutOfBoundsException) {
                    _songInfoState.value = _songInfoState.value.copy(
                        importedFileName = UiText.StringResource(R.string.file_name_hint)
                    )
                }
            }

            is UpdateSongName -> {
                _songInfoState.value = _songInfoState.value.copy(
                    songName = newName
                )
            }

            is UpdateSongArtistName -> {
                _songInfoState.value = _songInfoState.value.copy(
                    songArtistName = newName
                )
            }

            is ResetCounter -> {
                counter = 0
            }

            is IncreaseCounter -> {
                counter++
            }

            else -> Unit
        }
    }

    /**
     * Add to the imported songs state
     * @param value The new imported song
     * @author MaskedRedstonerProZ
     */
    fun addToImportedSongsState(index: Int, value: File) {
        _importedSongsState.add(
            index = index,
            element = value
        )
    }

    /**
     * Collect the song info from the states and add the song to the database
     * @author MaskedRedstonerProZ
     */
    fun addSong(songFilePath: String) {
        viewModelScope.launch {
            songUseCases.addSong(
                Song(
                    name = _songInfoState.value.songName,
                    artist = _songInfoState.value.songArtistName,
                    storagePath = songFilePath
                )
            )
        }
    }
}