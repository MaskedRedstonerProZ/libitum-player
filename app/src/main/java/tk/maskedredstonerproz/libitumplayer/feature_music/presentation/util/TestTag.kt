package tk.maskedredstonerproz.libitumplayer.feature_music.presentation.util

/**
 * Composable tags used for testing
 * @param tag The test tag
 * @author MaskedRedstonerProZ
 */
sealed class TestTag(private val tag: String) {

    /**
     * Test tag for the main screen
     * @author MaskedRedstonerProZ
     */
    object MainScreenTestTag: TestTag("main_screen")

    /**
     * Test tag for the song import screen
     * @author MaskedRedstonerProZ
     */
    object SongImportScreenTestTag: TestTag("song_import_screen")

    /**
     * Test tag for the about screen
     * @author MaskedRedstonerProZ
     */
    object AboutScreenTestTag: TestTag("about_screen")

    /**
     * Test tag for the about screen logo
     * @author MaskedRedstonerProZ
     */
    object AboutScreenLogoTestTag: TestTag("about_screen_logo")

    /**
     * Test tag for the about screen name
     * @author MaskedRedstonerProZ
     */
    object AboutScreenNameTestTag: TestTag("about_screen_name")

    /**
     * Test tag for the about screen paragraph
     * @author MaskedRedstonerProZ
     */
    object AboutScreenParagraphTestTag: TestTag("about_screen_paragraph")

    /**
     * Test tag for the app bar
     * @author MaskedRedstonerProZ
     */
    object AppBarTestTag: TestTag("app_bar")

    /**
     * Test tag for the app bar info
     * @author MaskedRedstonerProZ
     */
    object AppBarInfoTestTag: TestTag("app_bar_info")

    /**
     * Test tag for the loop song button in the app bar
     * @author MaskedRedstonerProZ
     */
    object LoopSongButtonTestTag: TestTag("loop_song_button")

    /**
     * Test tag for the loop song list button in the app bar
     * @author MaskedRedstonerProZ
     */
    object LoopSongListButtonTestTag: TestTag("loop_song_list_button")

    /**
     * Test tag for the shuffle songs button in the app bar
     * @author MaskedRedstonerProZ
     */
    object ShuffleSongsButtonTestTag: TestTag("shuffle_songs_button")

    /**
     * Test tag for the logo in the navigation drawer
     * @author MaskedRedstonerProZ
     */
    object NavigationDrawerLogoTestTag: TestTag("navigation_drawer_logo")

    /**
     * Test tag for the name in the navigation drawer
     * @author MaskedRedstonerProZ
     */
    object NavigationDrawerNameTestTag: TestTag("navigation_drawer_name")

    /**
     * Test tag for the main screen navigation menu entry
     * @author MaskedRedstonerProZ
     */
    object MainScreenMenuItemTestTag: TestTag("main_screen_menu_item")

    /**
     * Test tag for the song import screen navigation menu entry
     * @author MaskedRedstonerProZ
     */
    object SongImportScreenMenuItemTestTag: TestTag("song_import_screen_menu_item")

    /**
     * Test tag for the import button
     * @author MaskedRedstonerProZ
     */
    object ImportButtonTestTag: TestTag("import_button")

    /**
     * Test tag for the about screen navigation menu entry
     * @author MaskedRedstonerProZ
     */
    object AboutScreenMenuItemTestTag: TestTag("about_screen_menu_item")

    /**
     * Invoke function that allows calling the class + it's objects as functions
     * @return The test tag of a given item
     * @author MaskedRedstonerProZ
     */
    open operator fun invoke(): String {
        return this.tag
    }
}