package tk.maskedredstonerproz.libitumplayer.feature_music.domain.model

/**
 * State for keeping track of what play option is active
 * @author MaskedRedstonerProZ
 * @param repeatSong If the current song should be repeated by exoplayer
 * @param loopList If exoplayer should loop through the list of songs
 * @param shuffleSongs If exoplayer should play the songs in a random order
 */
data class SongPlayOptionState(
    val repeatSong: Boolean = false,
    val loopList: Boolean = true,
    val shuffleSongs: Boolean = false
)