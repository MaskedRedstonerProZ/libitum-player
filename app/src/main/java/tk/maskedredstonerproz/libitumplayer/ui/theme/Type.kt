package tk.maskedredstonerproz.libitumplayer.ui.theme

import androidx.compose.material.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import tk.maskedredstonerproz.libitumplayer.R

/**
 * Robotto font
 * @author Christian Robertson
 */
val roboto = FontFamily(
    Font(R.font.roboto_regular,FontWeight.Normal)
)

/**
 * My custom typography
 * @author MaskedRedstonerProZ
 */
val Typography = Typography(
    h1 = TextStyle(
        fontFamily = roboto,
        fontWeight = FontWeight.Normal,
        fontSize = 48.sp
    ),
    body1 = TextStyle(
        fontFamily = roboto,
        fontWeight = FontWeight.Normal,
        color = TextWhite,
        fontSize = 24.sp
    ),
    body2 = TextStyle(
        fontFamily = roboto,
        color = TextWhite,
        fontWeight = FontWeight.Normal,
        fontSize = 18.sp
    ),
    caption = TextStyle(
        fontFamily = roboto,
        fontWeight = FontWeight.Normal,
        color = TextWhite,
        fontSize = 15.sp
    )
)