package tk.maskedredstonerproz.libitumplayer.feature_music.presentation.screens.main_screen

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import kotlinx.coroutines.CoroutineScope
import org.koin.java.KoinJavaComponent.inject
import tk.maskedredstonerproz.libitumplayer.R
import tk.maskedredstonerproz.libitumplayer.feature_music.framework.exoplayer.isPlaying
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.components.StandardAppBar
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.util.Event.*
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.util.Screen.*
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.util.TestTag.*
import tk.maskedredstonerproz.libitumplayer.ui.theme.TextWhite

/**
 * Screen composable for the main song-playing screen
 * @author MaskedRedstonerProZ
 */
@OptIn(ExperimentalFoundationApi::class)
@Composable
fun MainScreen(
    scope: CoroutineScope,
    drawerState: DrawerState,
    navController: NavController
) {
    /**
     * [MainViewModel] instance injected by koin
     * @author MaskedRedstonerProZ
     */
    val mainViewModel: MainViewModel by inject(MainViewModel::class.java)

    /**
     * Root column composable containing the rest of the column ui
     * @author MaskedRedstonerProZ
     */
    Column(
        Modifier
            .fillMaxSize()
            .background(MaterialTheme.colors.background)
            .testTag(MainScreenTestTag())
    ) {
        /**
         * The standard app bar on the top of every screen, with all it's params
         * @author MaskedRedstonerProZ
         */
        StandardAppBar(
            screen = MainScreen,
            modifier = Modifier.testTag(AppBarTestTag()),
            scope = scope,
            drawerState = drawerState,
            playOptions = {
                /**
                 * Root row composable of the bar that houses the play option buttons
                 * @author MaskedRedstonerProZ
                 */
                Row {
                    /**
                     * Repeat song icon button, with all it's params
                     * @author MaskedRedstonerProZ
                     */
                    IconButton(
                        onClick = {
                            // set the play option state repeat song attribute to it's current opposite
                            mainViewModel.onEvent(ToggleSongLooping)
                        },
                        // set the test tag, and the dynamic background so the button looks "active"
                        modifier = Modifier
                            .testTag(LoopSongButtonTestTag())
                            .clip(MaterialTheme.shapes.medium)
                            .background(
                                if (mainViewModel.playOptionState.repeatSong) {
                                    MaterialTheme.colors.surface
                                } else Color.Transparent
                            )
                    ) {
                        // set the button's icon
                        Icon(
                            painter = painterResource(
                                id = R.drawable.ic_loop_song
                            ),
                            contentDescription = stringResource(
                                R.string.repeat_song
                            )
                        )
                    }

                    /**
                     * Loop list icon button, with all it's params
                     * @author MaskedRedstonerProZ
                     */
                    IconButton(
                        onClick = {
                            // set the play option state loop list attribute to it's current opposite
                            mainViewModel.onEvent(ToggleSongListLooping)
                        },
                        // set the test tag, and the dynamic background so the button looks "active"
                        modifier = Modifier
                            .testTag(LoopSongListButtonTestTag())
                            .clip(MaterialTheme.shapes.medium)
                            .background(
                                if (mainViewModel.playOptionState.loopList) {
                                    MaterialTheme.colors.surface
                                } else Color.Transparent
                            )
                    ) {
                        // set the button's icon
                        Icon(
                            painter = painterResource(
                                id = R.drawable.ic_loop_list
                            ),
                            contentDescription = stringResource(
                                R.string.repeat_list
                            )
                        )
                    }

                    /**
                     * Shuffle songs icon button, with all it's params
                     * @author MaskedRedstonerProZ
                     */
                    IconButton(
                        onClick = {
                            // set the play option state shuffle songs attribute to it's current opposite
                            mainViewModel.onEvent(ToggleSongShuffling)
                        },
                        // set the test tag, and the dynamic background so the button looks "active"
                        modifier = Modifier
                            .testTag(ShuffleSongsButtonTestTag())
                            .clip(MaterialTheme.shapes.medium)
                            .background(
                                if (mainViewModel.playOptionState.shuffleSongs) {
                                    MaterialTheme.colors.surface
                                } else Color.Transparent
                            )
                    ) {
                        // set the button's icon
                        Icon(
                            painter = painterResource(
                                id = R.drawable.ic_shuffle_songs
                            ),
                            contentDescription = stringResource(
                                R.string.shuffle_songs
                            )
                        )
                    }
                }
            }
        )

        // song fetching function call
        mainViewModel.onEvent(ObtainSongs)

        // in the event that there are no songs to show, instruct the user to import a few
        if (mainViewModel.songState.songs.isEmpty()) {
            Column(
                modifier = Modifier
                    .weight(5f)
                    .padding(20.dp),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Row(
                    modifier = Modifier
                        .padding(5.dp)
                        .background(color = Color.Black, shape = MaterialTheme.shapes.medium),
                    horizontalArrangement = Arrangement.Center
                ) {
                    Text(
                        text = stringResource(
                            id = R.string.no_songs_error
                        ),
                        color = TextWhite,
                        modifier = Modifier
                            .padding(5.dp)
                    )
                }
                Button(
                    shape = MaterialTheme.shapes.medium,
                    modifier = Modifier
                        .background(
                            color = Color.Black,
                            shape = MaterialTheme.shapes.medium
                        )
                        .padding(
                            start = 5.dp,
                            end = 5.dp
                        ),
                    onClick = {
                        navController.navigate(SongImportScreen())
                    }
                ) {
                    Text(text = stringResource(id = R.string.btn_import))
                }
            }
            return@Column
        }

        // the list composable that contains the song items
        LazyColumn(Modifier.weight(5f)) {

            // the actual song items definition for each item
            items(
                items = mainViewModel.songState.songs,
                itemContent = { item ->

                    // root row that contains the ui for each item(with dynamic border that shows up when selected)
                    Row(
                        Modifier
                            .fillMaxSize()
                            .padding(start = 5.dp, end = 5.dp, top = 5.dp, bottom = 0.dp)
                            .clip(MaterialTheme.shapes.medium)
                            .background(Color.Black)
                            .border(
                                width = 5.dp,
                                mainViewModel.songState.selectedSongIndex.let {

                                    if (it != mainViewModel.songState.songs.indexOf(item)) {
                                        return@let Color.Transparent
                                    }

                                    MaterialTheme.colors.primary
                                }
                            )
                            .combinedClickable(
                                onClick = {

                                    /*
                                     * If the index of the song the user clicked on matches the index of the currently selected one,
                                     * set the index of the selected one to null
                                    */
                                    if (mainViewModel.songState.songs.indexOf(item) == mainViewModel.songState.selectedSongIndex) return@combinedClickable

                                    // And in every other case just set the index of the currently selected song to the one the user clicked on
                                    mainViewModel.onEvent(
                                        ToggleSongSelectedStatus(
                                            mainViewModel.songState.songs.indexOf(
                                                item
                                            )
                                        )
                                    )
                                    mainViewModel.onEvent(ToggleIsSongPlayingStatus)

                                },
                                onLongClick = {
                                    // remove the song from the database whenever it's long-clicked
                                    mainViewModel.deleteSong(item)
                                }
                            ),
                        verticalAlignment = Alignment.CenterVertically
                    ) {

                        // the item ui
                        MaterialTheme.colors.onPrimary.let { c ->
                            Image(painter = painterResource(id = R.drawable.ic_music),
                                contentDescription = "Music note",
                                Modifier
                                    .size(50.dp)
                                    .aspectRatio(1F),
                                colorFilter = MaterialTheme.colors.surface.let {
                                    ColorFilter.lighting(it, it)
                                }
                            )
                            Column(
                                Modifier
                                    .weight(weight = 2.3f, fill = true)
                                    .padding(5.dp)
                            ) {
                                Text(
                                    text = item.name,
                                    style = MaterialTheme.typography.body2,
                                    color = c
                                )
                                Text(
                                    text = item.artist,
                                    style = MaterialTheme.typography.caption,
                                    color = c
                                )
                            }
                            Text(
                                text = "Song number: ${
                                    mainViewModel.songState.songs.indexOf(
                                        item
                                    ) + 1
                                }",
                                style = MaterialTheme.typography.caption,
                                color = c,
                                modifier = Modifier
                                    .weight(
                                        weight = 1f,
                                        fill = true
                                    )
                            )
                        }
                    }
                }
            )
        }
        Column(
            Modifier
                .fillMaxWidth()
                .padding(5.dp)
                .clip(MaterialTheme.shapes.medium)
                .weight(1f)
        ) {
            Row(
                horizontalArrangement = Arrangement.SpaceEvenly,
                verticalAlignment = Alignment.CenterVertically
            ) {

                // the song progress indicator(with fast forward/rewind functionality)
                Row(
                    Modifier
                        .weight(5.4f)
                        .clip(MaterialTheme.shapes.medium)
                        .background(Color.Black)
                ) {
                    Slider(
                        value = mainViewModel.songState.currentSongPlayTime,
                        valueRange = 0.0F..100.0F,
                        onValueChange = {
                            mainViewModel.onEvent(UpdateSongPlayTime(it))
                        },
                        modifier = Modifier.padding(5.dp)
                    )
                }
                Spacer(modifier = Modifier.width(5.dp))

                // play/pause control
                Row(
                    Modifier
                        .weight(1f)
                        .clip(MaterialTheme.shapes.medium)
                        .background(Color.Black)
                ) {
                    Image(
                        painter = painterResource(
                            id = mainViewModel.musicServiceConnection.playbackState.value?.isPlaying.let {

                                if (it == null || it == false) {
                                    return@let R.drawable.ic_play
                                }

                                R.drawable.ic_pause
                            }
                        ),
                        contentDescription = stringResource(
                            R.string.play_pause
                        ),
                        modifier = Modifier
                            .aspectRatio(1f)
                            .padding(5.dp)
                            .clip(RoundedCornerShape(5.dp))
                            .clickable {
                                mainViewModel.onEvent(ToggleIsSongPlayingStatus)
                            }
                    )
                }
            }
            Spacer(modifier = Modifier.height(5.dp))

            // current play time in song display, current song name and artist display and song duration display
            Row(
                Modifier
                    .fillMaxSize()
                    .clip(MaterialTheme.shapes.medium)
                    .background(MaterialTheme.colors.onSecondary),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Spacer(Modifier.width(4.dp))
                Box(
                    Modifier
                        .aspectRatio(1F)
                        .clip(MaterialTheme.shapes.medium)
                        .background(Color.Black)
                        .weight(1F),
                    contentAlignment = Alignment.Center
                ) {
                    Text(
                        text = "00:00",
                        color = MaterialTheme.colors.onPrimary,
                        style = MaterialTheme.typography.caption,
                        modifier = Modifier.padding(5.dp)
                    )
                }
                Spacer(Modifier.width(5.dp))
                Row(
                    Modifier
                        .clip(MaterialTheme.shapes.medium)
                        .background(Color.Black)
                        .weight(6.5F),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.Center
                ) {
                    Text(
                        text = mainViewModel.songState.selectedSongIndex.let {

                            "${mainViewModel.songState.songs[it].artist} - ${mainViewModel.songState.songs[it].name}"
                        },
                        color = MaterialTheme.colors.onPrimary,
                        style = MaterialTheme.typography.caption,
                        modifier = Modifier.padding(5.dp)
                    )
                }
                Spacer(Modifier.width(5.dp))
                Box(
                    Modifier
                        .aspectRatio(1F)
                        .clip(MaterialTheme.shapes.medium)
                        .background(Color.Black)
                        .weight(1F),
                    contentAlignment = Alignment.Center
                ) {
                    Text(
                        text = "00:00",
                        color = MaterialTheme.colors.onPrimary,
                        style = MaterialTheme.typography.caption,
                        modifier = Modifier.padding(5.dp)
                    )
                }
                Spacer(Modifier.width(4.dp))
            }
        }
    }
}