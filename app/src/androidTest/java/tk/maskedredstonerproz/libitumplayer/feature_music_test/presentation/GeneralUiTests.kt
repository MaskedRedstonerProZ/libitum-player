package tk.maskedredstonerproz.libitumplayer.feature_music_test.presentation

import androidx.activity.compose.setContent
import androidx.compose.material.DrawerState
import androidx.compose.material.DrawerValue
import androidx.compose.material.rememberDrawerState
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.koin.core.context.loadKoinModules
import org.koin.core.context.unloadKoinModules
import tk.maskedredstonerproz.libitumplayer.di.testAppModule
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.MainActivity
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.util.Navigation
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.util.TestTag.*

/**
 * General ui test class
 * @author MaskedRedstonerProZ
 */
@LargeTest
class GeneralUiTests {

    /**
     * Compose test rule, aka compose environment required to test composables
     * @author MaskedRedstonerProZ
     */
    @get:Rule
    val composeTestRule = createAndroidComposeRule<MainActivity>()

    /**
     * Navigation host controller, obtained via rememberNavController()
     * @author MaskedRedstonerProZ
     */
    private lateinit var navController: NavHostController

    /**
     * Navigation drawer state, obtained via rememberDrawerState()
     * @author MaskedRedstonerProZ
     */
    private lateinit var drawerState: DrawerState

    /**
     * Coroutine scope, obtained via rememberCoroutineScope()
     * @author MaskedRedstonerProZ
     */
    private lateinit var scope: CoroutineScope

    /**
     * Pre-test function for initial setup
     * @author MaskedRedstonerProZ
     */
    @Before
    fun setUp() {
        composeTestRule.activity.setContent {
            navController = rememberNavController()
            drawerState = rememberDrawerState(DrawerValue.Closed)
            scope = rememberCoroutineScope()
        }

        loadKoinModules(testAppModule)
    }

    /**
     * Test that checks if the navigation drawer opens,
     * and allows users to switch between screens
     * @author MaskedRedstonerProZ
     */
    @Test
    fun doesNavigationWork() {
        composeTestRule.apply {

            // Compose the composable(s) to test
            setContent {
                Navigation(navController = navController, drawerState = drawerState)
            }

            // Open the navigation drawer via the coroutine scope and drawer state
            scope.launch {
                drawerState.open()
            }

            // Check if the drawer opens(by checking if the main screen navigation drawer entry exists)
            onNodeWithTag(MainScreenMenuItemTestTag()).assertExists()

            // Simulate a click on the song import screen navigation drawer entry
            onNodeWithTag(SongImportScreenMenuItemTestTag()).performClick()

            // Check if the song import screen has composed
            onNodeWithTag(SongImportScreenTestTag()).assertExists()

            // Open the navigation drawer via the coroutine scope and drawer state
            scope.launch {
                drawerState.open()
            }

            // Simulate a click on the about screen navigation drawer entry
            onNodeWithTag(AboutScreenMenuItemTestTag()).performClick()

            // Check if the about screen has composed
            onNodeWithTag(AboutScreenTestTag()).assertExists()
        }

    }

    /**
     * Simple test for making sure the testing software works,
     * gets the test app context from the registry,
     * and checks if the app's root package name is legit, which it always is
     * @author MaskedRedstonerProZ
     */
    @Test
    fun doesTestingSetupWork() {
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("tk.maskedredstonerproz.libitumplayer", appContext.packageName)
    }

    /**
     * Post-test function for undoing initial setup
     * @author MaskedRedstonerProZ
     */
    @After
    fun tearDown() {
        unloadKoinModules(testAppModule)
    }
}