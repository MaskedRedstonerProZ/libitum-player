package tk.maskedredstonerproz.libitumplayer.feature_music_test.presentation

import androidx.activity.compose.setContent
import androidx.compose.material.DrawerState
import androidx.compose.material.DrawerValue
import androidx.compose.material.rememberDrawerState
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import androidx.test.filters.LargeTest
import kotlinx.coroutines.CoroutineScope
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.koin.core.context.loadKoinModules
import org.koin.core.context.unloadKoinModules
import tk.maskedredstonerproz.libitumplayer.di.testAppModule
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.MainActivity
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.screens.main_screen.MainScreen
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.util.Navigation
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.util.TestTag.*

/**
 * [MainScreen] ui test class
 * @author MaskedRedstonerProZ
 */
@LargeTest
class MainScreenTests {

    /**
     * Compose test rule, aka compose environment required to test composables
     * @author MaskedRedstonerProZ
     */
    @get:Rule
    val composeTestRule = createAndroidComposeRule<MainActivity>()

    /**
     * Navigation host controller, obtained via rememberNavController()
     * @author MaskedRedstonerProZ
     */
    private lateinit var navController: NavHostController

    /**
     * Navigation drawer state, obtained via rememberDrawerState()
     * @author MaskedRedstonerProZ
     */
    private lateinit var drawerState: DrawerState

    /**
     * Coroutine scope, obtained via rememberCoroutineScope()
     * @author MaskedRedstonerProZ
     */
    private lateinit var scope: CoroutineScope

    /**
     * Pre-test function for initial setup
     * @author MaskedRedstonerProZ
     */
    @Before
    fun setUp() {
        composeTestRule.activity.setContent {
            navController = rememberNavController()
            drawerState = rememberDrawerState(DrawerValue.Closed)
            scope = rememberCoroutineScope()
        }

        loadKoinModules(testAppModule)
    }

    /**
     * Test that checks if the play option buttons show up
     * @author MaskedRedstonerProZ
     */
    @Test
    fun doPlayOptionButtonsShowUp() {
        composeTestRule.apply {

            // Compose the composable(s) to test
            setContent {
                Navigation(navController = navController, drawerState = drawerState)
            }

            // Check if the loop song button has composed
            onNodeWithTag(LoopSongButtonTestTag()).assertExists()

            // Check if the loop song list button has composed
            onNodeWithTag(LoopSongListButtonTestTag()).assertExists()

            // Check if the shuffle songs button has composed
            onNodeWithTag(ShuffleSongsButtonTestTag()).assertExists()
        }
    }

    /**
     * Post-test function for undoing initial setup
     * @author MaskedRedstonerProZ
     */
    @After
    fun tearDown() {
        unloadKoinModules(testAppModule)
    }
}