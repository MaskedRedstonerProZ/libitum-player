package tk.maskedredstonerproz.libitumplayer.feature_music.data.db

import androidx.room.*
import tk.maskedredstonerproz.libitumplayer.feature_music.domain.model.Song

/**
 * [SongDatabase] access object for interfacing with the database
 * @author MaskedRedstonerProZ
 */
@Dao
interface SongDao {

    /**
     * Returns a [List] of type [Song] from the database
     * @author MaskedRedstonerProZ
     */
    @Query("SELECT * FROM song")
    fun loadSongs(): List<Song>

    /**
     * Returns a [Song] determined by the specified id
     * @param id The id of the required song
     * @author MaskedRedstonerProZ
     */
    @Query("SELECT * FROM song WHERE id = :id")
    suspend fun getSongById(id: Int): Song?

    /**
     * Inserts a [Song] into the database
     * @param song The [Song] to insert
     * @author MaskedRedstonerProZ
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSong(song: Song)

    /**
     * Removes a [Song] from the database
     * @param song The [Song] to remove
     * @author MaskedRedstonerProZ
     */
    @Delete
    suspend fun deleteSong(song: Song)
}