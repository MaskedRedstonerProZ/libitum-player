package tk.maskedredstonerproz.libitumplayer.feature_music.presentation.screens.main_screen

import android.support.v4.media.MediaBrowserCompat
import android.support.v4.media.MediaMetadataCompat.METADATA_KEY_MEDIA_ID
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import tk.maskedredstonerproz.libitumplayer.feature_music.domain.exoplayer.service.MusicServiceConnection
import tk.maskedredstonerproz.libitumplayer.feature_music.domain.model.Song
import tk.maskedredstonerproz.libitumplayer.feature_music.domain.model.SongPlayOptionState
import tk.maskedredstonerproz.libitumplayer.feature_music.domain.model.SongState
import tk.maskedredstonerproz.libitumplayer.feature_music.domain.use_case.SongUseCases
import tk.maskedredstonerproz.libitumplayer.feature_music.framework.exoplayer.isPlayEnabled
import tk.maskedredstonerproz.libitumplayer.feature_music.framework.exoplayer.isPlaying
import tk.maskedredstonerproz.libitumplayer.feature_music.framework.exoplayer.isPrepared
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.util.Constants.MEDIA_ROOT_ID
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.util.Event
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.util.Event.*

/**
 * [MainViewModel] class for managing the states,
 * and general ui interaction
 * @param songUseCases The [SongUseCases] instance for accessing the database
 * @param musicServiceConnection The [MusicServiceConnection] instance for interacting with the service
 * @author MaskedRedstonerProZ
 */
class MainViewModel(
    private val songUseCases: SongUseCases,
    val musicServiceConnection: MusicServiceConnection
) : ViewModel() {

    /**
     * The state for the play option buttons
     * @author MaskedRedstonerProZ
     */
    private val _playOptionState = mutableStateOf(SongPlayOptionState())
    val playOptionState by _playOptionState

    /**
     * The state that determines if a song is playing or not
     * @author MaskedRedstonerProZ
     */
    private val _songState = mutableStateOf(SongState())
    val songState by _songState

    /**
     * Handles events sent to the [MainViewModel] from the [MainScreen]
     * @param event The event to handle
     * @author MaskedRedstonerProZ
     */
    fun onEvent(event: Event) = event.apply {

        when (this) {

            is ToggleSongLooping -> {
                if(_playOptionState.value.repeatSong) return@apply

                _playOptionState.value = SongPlayOptionState(
                    repeatSong = !_playOptionState.value.repeatSong
                )
            }

            is ToggleSongListLooping -> {
                if(_playOptionState.value.loopList) return@apply

                _playOptionState.value = SongPlayOptionState(
                    loopList = !_playOptionState.value.loopList
                )
            }

            is ToggleSongShuffling -> {
                if(_playOptionState.value.shuffleSongs) return@apply

                _playOptionState.value = SongPlayOptionState(
                    shuffleSongs = !_playOptionState.value.shuffleSongs
                )
            }

            is ObtainSongs -> {
                musicServiceConnection.subscribe(MEDIA_ROOT_ID, object : MediaBrowserCompat.SubscriptionCallback() {

                    override fun onChildrenLoaded(
                        parentId: String,
                        children: MutableList<MediaBrowserCompat.MediaItem>
                    ) {
                        super.onChildrenLoaded(parentId, children)
                        val songs = children.map {
                            Song(
                                name = it.description.title.toString(),
                                artist = it.description.subtitle.toString(),
                                storagePath = it.description.mediaUri.toString(),
                                id = it.mediaId!!.toInt()
                            )
                        }
                        _songState.value = _songState.value.copy(
                            songs = songs.sortedBy { it.artist }
                        )
                    }



                })
            }

            is ToggleSongSelectedStatus -> {
                _songState.value = _songState.value.copy(
                    selectedSongIndex = index
                )
            }

            is ToggleIsSongPlayingStatus -> {
                playOrToggleSong(_songState.value.songs[_songState.value.selectedSongIndex])
            }

            is UpdateSongPlayTime -> {
                _songState.value = _songState.value.copy(
                    currentSongPlayTime = newTime
                )
            }

            is UpdateSongDuration -> {
                _songState.value = _songState.value.copy(
                    currentSongDuration = newSongDuration
                )
            }

            else -> Unit
        }
    }

    private fun playOrToggleSong(mediaItem: Song) {
        val isPrepared = musicServiceConnection.playbackState.value?.isPrepared ?: false
        if(isPrepared && mediaItem.id.toString() ==
            musicServiceConnection.currentPlayingSong.value?.getString(METADATA_KEY_MEDIA_ID)) {
            musicServiceConnection.playbackState.value?.let { playbackState ->
                when {
                    playbackState.isPlaying -> musicServiceConnection.transportControls.pause()
                    playbackState.isPlayEnabled -> musicServiceConnection.transportControls.play()
                    else -> Unit
                }
            }
        } else {
            musicServiceConnection.transportControls.playFromMediaId(mediaItem.id.toString(), null)
        }
    }

    /**
     * Deletes a [Song] from the database
     * @param song The [Song] to remove
     * @author MaskedRedstonerProZ
     */
    fun deleteSong(song: Song) {
        viewModelScope.launch {
            songUseCases.deleteSong(song)
            _songState.value.songs.drop(_songState.value.songs.indexOf(song))
        }
    }

    override fun onCleared() {
        super.onCleared()
        musicServiceConnection.unsubscribe(MEDIA_ROOT_ID, object : MediaBrowserCompat.SubscriptionCallback() {})
    }
}