package tk.maskedredstonerproz.libitumplayer.feature_music.domain.model

/**
 * The state for keeping track of songs,
 * and their associated info
 * @param songs The list of [Song] objects
 * @param selectedSongIndex The index of the [Song] object selected by the user
 * currently or not
 * @param currentSongPlayTime The elapsed time since the song started playing
 * @param currentSongDuration The duration of the currently playing song
 * @author MaskedRedstonerProZ
 */
data class SongState(
    val songs: List<Song> = emptyList(),
    val selectedSongIndex: Int = 0,
    val currentSongPlayTime: Float = 0.0F,
    val currentSongDuration: Float = 0.0F
)
