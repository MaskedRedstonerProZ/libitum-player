package tk.maskedredstonerproz.libitumplayer.feature_music.presentation.screens.about_screen

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.DrawerState
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.CoroutineScope
import tk.maskedredstonerproz.libitumplayer.R
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.components.StandardAppBar
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.util.Screen.*
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.util.TestTag.*
import tk.maskedredstonerproz.libitumplayer.ui.theme.TextWhite

/**
 * Screen composable for the about info screen
 * @author MaskedRedstonerProZ
 */
@Composable
fun AboutScreen(
    scope: CoroutineScope,
    drawerState: DrawerState
) {
    /**
     * Root column composable containing the rest of the column ui
     * @author MaskedRedstonerProZ
     */
    Column(
        Modifier
            .fillMaxSize()
            .testTag(AboutScreenTestTag())
            .background(MaterialTheme.colors.background),
    ) {
        /**
         * The standard app bar on the top of every screen, with all it's params
         * @author MaskedRedstonerProZ
         */
        StandardAppBar(
            screen = AboutScreen,
            modifier = Modifier.testTag(AppBarTestTag()),
            scope = scope,
            drawerState = drawerState
        )
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(10.dp),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Spacer(modifier = Modifier.height(10.dp))

            // logo
            Row(
                modifier = Modifier
                    .padding(5.dp)
                    .background(color = Color.Black, shape = MaterialTheme.shapes.medium),
                horizontalArrangement = Arrangement.Center
            ) {
                Image(
                    painter = painterResource(id = R.drawable.ic_logo),
                    contentDescription = "logo",
                    modifier = Modifier
                        .padding(10.dp)
                        .testTag(AboutScreenLogoTestTag())
                )
            }

            // name
            Row(
                modifier = Modifier
                    .padding(5.dp)
                    .background(color = Color.Black, shape = MaterialTheme.shapes.medium),
                horizontalArrangement = Arrangement.Center
            ) {
                Text(
                    text = stringResource(
                        id = R.string.name
                    ),
                    color = TextWhite,
                    modifier = Modifier
                        .padding(5.dp)
                        .testTag(AboutScreenNameTestTag())
                )
            }

            // about info
            Row(
                modifier = Modifier
                    .padding(5.dp)
                    .background(color = Color.Black, shape = MaterialTheme.shapes.medium),
                horizontalArrangement = Arrangement.Center
            ) {
                Text(
                    text = stringResource(
                        id = R.string.about_info
                    ),
                    style = MaterialTheme.typography.caption,
                    modifier = Modifier
                        .padding(10.dp)
                        .testTag(AboutScreenParagraphTestTag())
                )
            }
        }
    }
}