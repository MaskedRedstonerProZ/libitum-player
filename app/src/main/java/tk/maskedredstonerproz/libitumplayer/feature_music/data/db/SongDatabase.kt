package tk.maskedredstonerproz.libitumplayer.feature_music.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import tk.maskedredstonerproz.libitumplayer.feature_music.domain.model.Song

/**
 * The [SongDatabase] class that describes the database
 * @author MaskedRedstonerProZ
 */
@Database(
    entities = [Song::class],
    version = 10,
    exportSchema = false
)
abstract class SongDatabase: RoomDatabase() {

    /**
     * [SongDao] instance for
     * interfacing with the [SongDatabase]
     * @author MaskedRedstonerProZ
     */
    abstract val dao: SongDao

}