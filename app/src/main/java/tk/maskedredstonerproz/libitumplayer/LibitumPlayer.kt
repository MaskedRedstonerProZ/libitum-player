package tk.maskedredstonerproz.libitumplayer

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import tk.maskedredstonerproz.libitumplayer.di.appModule
import tk.maskedredstonerproz.libitumplayer.di.serviceModule

/**
 * Application class required for dependency injection setup
 * @author MaskedRedstonerProZ
 */
class LibitumPlayer: Application() {

    /**
     * First function in application lifecycle, called when the
     * application instance is created
     * @author MaskedRedstonerProZ
     */
    override fun onCreate() {
        super.onCreate()

        // Start the KoinApplication required for Koin dependency injection to work
        startKoin {

            // Set the android logger level to only log errors and info with less priority
            androidLogger(Level.ERROR)

            // Set the android context to the active instance of this class
            androidContext(this@LibitumPlayer)

            // Set the module(s) for Koin to get the injectable dependencies from
            modules(
                listOf(appModule, serviceModule)
            )
        }
    }
}