package tk.maskedredstonerproz.libitumplayer.di

import org.koin.dsl.module
import org.koin.java.KoinJavaComponent.inject
import tk.maskedredstonerproz.libitumplayer.feature_music.domain.repo.SongRepository
import tk.maskedredstonerproz.libitumplayer.feature_music.domain.use_case.*
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.screens.main_screen.MainViewModel
import tk.maskedredstonerproz.libitumplayer.feature_music_test.domain.repo.SongRepositoryTestImpl
/**
 * Test application module used for injecting test dependencies
 * @author MaskedRedstonerProZ
 */
val testAppModule = module(override = true) {

    single<SongRepository> {
        SongRepositoryTestImpl()
    }

    val repository: SongRepository by inject(SongRepository::class.java)

    single {
        SongUseCases(
            getSongs = GetSongs(repository),
            deleteSong = DeleteSong(repository),
            addSong = AddSong(repository),
            getSong = GetSong(repository)
        )
    }

    val songUseCases: SongUseCases by inject(SongUseCases::class.java)

    single {
        MainViewModel(songUseCases)
    }
}