package tk.maskedredstonerproz.libitumplayer.feature_music.presentation.util

import tk.maskedredstonerproz.libitumplayer.feature_music.domain.model.*
import tk.maskedredstonerproz.libitumplayer.feature_music.domain.exoplayer.service.*

/**
 * The [Event] class for easy definition of events
 * @author MaskedRedstonerProZ
 */
sealed class Event {

    // Main Screen

    /**
     * The [Event] that fires whenever the single [Song] looping feature is toggled on or off
     * @author MaskedRedstonerProZ
     */
    object ToggleSongLooping: Event()

    /**
     * The [Event] that fires whenever the [Song] list looping feature is toggled on or off
     * @author MaskedRedstonerProZ
     */
    object ToggleSongListLooping: Event()

    /**
     * The [Event] that fires whenever the [Song] list shuffling feature is toggled on or off
     * @author MaskedRedstonerProZ
     */
    object ToggleSongShuffling: Event()

    /**
     * The [Event] that fires whenever the [Song] list is obtained
     * @author MaskedRedstonerProZ
     */
    object ObtainSongs: Event()

    /**
     * The [Event] that fires whenever the selected status of a [Song] is toggled
     * @param index The index of the selected [Song] in the list
     * @author MaskedRedstonerProZ
     */
    data class ToggleSongSelectedStatus(val index: Int): Event()

    /**
     * The [Event] that fires whenever the [Song] playing status is toggled
     * @author MaskedRedstonerProZ
     */
    object ToggleIsSongPlayingStatus: Event()

    /**
     * The [Event] that fires whenever the [Song] play time is updated
     * @param newTime The new [Song] play time
     * @author MaskedRedstonerProZ
     */
    data class UpdateSongPlayTime(val newTime: Float): Event()

    /**
     * The [Event] that fires whenever the [Song] duration is updated
     * @param newSongDuration The new [Song] duration
     * @author MaskedRedstonerProZ
     */
    data class UpdateSongDuration(val newSongDuration: Float): Event()

    // Song Import Screen

    /**
     * The [Event] that fires whenever the imported file name is updated
     * @param useInitialValue The determinate if the initial value of
     * the imported file name should be used in the update
     * @author MaskedRedstonerProZ
     */
    data class UpdateImportedFileName(val useInitialValue: Boolean): Event()

    /**
     * The [Event] that fires whenever the [Song] name is updated
     * @param newName The new [Song] name
     * @author MaskedRedstonerProZ
     */
    data class UpdateSongName(val newName: String): Event()

    /**
     * The [Event] that fires whenever the [Song] artist name is updated
     * @param newName The new [Song] artist name
     * @author MaskedRedstonerProZ
     */
    data class UpdateSongArtistName(val newName: String): Event()

    /**
     * The [Event] that fires whenever the counter is reset
     * @author MaskedRedstonerProZ
     */
    object ResetCounter: Event()

    /**
     * The [Event] that fires whenever the counter is increased
     * @author MaskedRedstonerProZ
     */
    object IncreaseCounter: Event()

    /**
     * The [Event] class that allows easy definition of [MusicServiceConnection] events
     * @author MaskedRedstonerProZ
     */
    class MusicServiceConnectionEvent<out T>(val status: Status, val data: T? = null, message: String? = null): Event() {

        companion object {
            /**
             * The [MusicServiceConnectionEvent] that fires whenever the [MusicServiceConnection] successfully connects
             * @author MaskedRedstonerProZ
             */
            fun <T> success(data: T? = null) = MusicServiceConnectionEvent(Status.SUCCESS, data)

            /**
             * The [MusicServiceConnectionEvent] that fires whenever there's an error upon connecting to the [MusicServiceConnection]
             * @author MaskedRedstonerProZ
             */
            fun <T> error(message: String, data: T? = null) =
                MusicServiceConnectionEvent(Status.ERROR, data, message)

            /**
             * The [MusicServiceConnectionEvent] that fires whenever the [Song] data from the [MusicServiceConnection] starts loading
             * @author MaskedRedstonerProZ
             */
            fun <T> loading(data: T? = null) = MusicServiceConnectionEvent(Status.LOADING, data)
        }

        /**
         * Determines if the [MusicServiceConnectionEvent] has been handled
         * @author MaskedRedstonerProZ
         */
        var hasBeenHandled = false
            private set

        /**
         * Gets the data in case the [MusicServiceConnectionEvent] hasn't been handled
         * @return Nullable data of type [T]
         * @author MaskedRedstonerProZ
         */
        fun getDataIfNotHandled(): T? {
            return if(hasBeenHandled) {
                null
            } else {
                hasBeenHandled = true
                data
            }
        }

//        /**
//         * Allows "peeking" of the data
//         * @author MaskedRedstonerProZ
//         */
//        fun peekContent() = data
    }
}
