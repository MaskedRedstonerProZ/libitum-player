package tk.maskedredstonerproz.libitumplayer.ui.theme

import androidx.compose.ui.graphics.Color

/**
 * My custom colour scheme
 * @author MaskedRedstonerProZ
 */
val DarkGray = Color(0xFF30343F)
val LightGray = Color(0xFF7A7B83)
val TextWhite = Color(0xFFF7F0F5)
val RedAccent = Color(0xFFC80000)
val BlueAccent = Color(0xFF6184D8)