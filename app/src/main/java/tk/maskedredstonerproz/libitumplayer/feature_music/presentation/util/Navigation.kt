package tk.maskedredstonerproz.libitumplayer.feature_music.presentation.util

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import kotlinx.coroutines.launch
import tk.maskedredstonerproz.libitumplayer.R
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.screens.about_screen.AboutScreen
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.screens.main_screen.MainScreen
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.screens.song_import_screen.SongImportScreen
import tk.maskedredstonerproz.libitumplayer.ui.theme.DarkGray
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.util.TestTag.*

/**
 * Navigation composable that allows navigation between screens,
 * and defines how that navigation happens. As well as adds a navigation drawer
 * @param navController Navigation controller used for the actual navigation
 * @param drawerState The state of the navigation drawer
 * @author MaskedRedstonerProZ
 */
@Composable
fun Navigation(
    navController: NavHostController,
    drawerState: DrawerState
) {
    val scope = rememberCoroutineScope()
    val testTagArray = arrayOf(
        MainScreenMenuItemTestTag,
        SongImportScreenMenuItemTestTag,
        AboutScreenMenuItemTestTag
    )
    ModalDrawer(
        drawerContent = {
            // root column that houses the drawer content
            Column(
                Modifier
                    .fillMaxSize()
                    .background(MaterialTheme.colors.surface)) {

                Spacer(modifier = Modifier.height(10.dp))

                // logo
                Row(
                    Modifier
                        .fillMaxWidth()
                        .testTag(NavigationDrawerLogoTestTag()),
                    horizontalArrangement = Arrangement.Center
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.ic_logo),
                        contentDescription = "logo"
                    )
                }

                // name
                Row(
                    Modifier
                        .fillMaxWidth()
                        .testTag(NavigationDrawerNameTestTag()),
                    horizontalArrangement = Arrangement.Center
                ) {
                    Text(
                        text = stringResource(
                            id = R.string.name
                        ),
                        color = DarkGray
                    )
                }

                Spacer(modifier = Modifier.height(5.dp))

                // navigation items
                arrayOf(
                    Screen.MainScreen,
                    Screen.SongImportScreen,
                    Screen.AboutScreen
                ).forEachIndexed { i, it ->

                    // the navigation item design
                    Row(
                        Modifier
                            .fillMaxWidth()
                            .padding(start = 5.dp, end = 5.dp, top = 5.dp)
                            .clip(MaterialTheme.shapes.medium)
                            .background(MaterialTheme.colors.primary)
                            .testTag(testTagArray[i]())
                            .clickable {
                                if (navController.currentDestination?.route != it()) {
                                    navController.apply {
                                        popBackStack()
                                        navigate(it())

                                        scope.launch {
                                            drawerState.close()
                                        }

                                        return@clickable
                                    }
                                }

                                scope.launch {
                                    drawerState.close()
                                }
                            }
                    ) {
                        Text(text = "- ${it.name}", color = MaterialTheme.colors.onSecondary, modifier = Modifier
                            .padding(start = 5.dp)
                        )
                    }
                }
            }
    },
        drawerShape = MaterialTheme.shapes.small,
        scrimColor = Color.Transparent,
        drawerState = drawerState
    ) {
        /*
         * Call the NavHost composable, set it's navigation controller to the one
         * passed into the Navigation composable,
         * then set the starting route to the route of MainScreen
         */
        NavHost(
            navController = navController,
            startDestination = Screen.MainScreen()
        ) {

            // Define the navigation backstack entry for the MainScreen route
            composable(Screen.MainScreen()) {
                MainScreen(
                    scope = scope,
                    drawerState = drawerState,
                    navController = navController
                )
            }

            // Define the navigation backstack entry for the SongImportScreen route
            composable(Screen.SongImportScreen()) {
                SongImportScreen(
                    scope = scope,
                    drawerState = drawerState
                )
            }

            // Define the navigation backstack entry for the AboutScreen route
            composable(Screen.AboutScreen()) {
                AboutScreen(
                    scope = scope,
                    drawerState = drawerState
                )
            }
        }
    }
}