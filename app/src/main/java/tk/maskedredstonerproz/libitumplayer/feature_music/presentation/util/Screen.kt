package tk.maskedredstonerproz.libitumplayer.feature_music.presentation.util

/**
 * [Screen] class for easy definition of screen routes,
 * and work with said routes
 * @param route The route of a given screen
 * @param name The display name of a given screen
 * @author MaskedRedstonerProZ
 */
sealed class Screen(private val route: String, val name: String) {

    /**
     * MainScreen screen object
     * @author MaskedRedstonerProZ
     */
    object MainScreen : Screen(route = "main_screen", name = "Home")

    /**
     * SongImportScreen screen object
     * @author MaskedRedstonerProZ
     */
    object SongImportScreen : Screen(route = "song_import_screen", name = "Song Import")

    /**
     * AboutScreen screen object
     * @author MaskedRedstonerProZ
     */
    object AboutScreen : Screen(route = "about_screen", name = "About")

    /**
     * Invoke function that allows calling the class + it's objects as functions
     * @return The screen's route
     * @author MaskedRedstonerProZ
     */
    open operator fun invoke(): String {
        return this.route
    }

    companion object {
        fun getScreenByRoute(route: String): Screen {

            when (route) {

                MainScreen() -> {
                    return MainScreen
                }

                SongImportScreen() -> {
                    return SongImportScreen
                }

                AboutScreen() -> {
                    return AboutScreen
                }

                else -> throw InvalidRouteException("You've supplied an invalid route idiot!!")
            }
        }
    }

    private class InvalidRouteException(message: String?) : Exception(message)
}