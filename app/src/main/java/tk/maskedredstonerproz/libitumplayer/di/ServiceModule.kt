package tk.maskedredstonerproz.libitumplayer.di

import com.google.android.exoplayer2.C.AUDIO_CONTENT_TYPE_MUSIC
import com.google.android.exoplayer2.C.USAGE_MEDIA
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.audio.AudioAttributes
import com.google.android.exoplayer2.upstream.DefaultDataSource
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import org.koin.java.KoinJavaComponent.inject
import tk.maskedredstonerproz.libitumplayer.feature_music.data.db.SongDatabase
import tk.maskedredstonerproz.libitumplayer.feature_music.framework.exoplayer.LocalMusicSource
import tk.maskedredstonerproz.libitumplayer.feature_music.framework.exoplayer.service.MusicService

/**
 * Service module for injecting dependencies
 * @author MaskedRedstonerProZ
 */
val serviceModule = module {

    /**
     * [SongDatabase] instance injected by koin
     * @author MaskedRedstonerProZ
     */
    val db: SongDatabase by inject(SongDatabase::class.java)

    /**
     * Service scope that allows our dependencies to be scoped
     * to the service
     * @author MaskedRedstonerProZ
     */
    scope<MusicService> {

//        /**
//         * [AudioAttributes] dependency definition for injection
//         * @author MaskedRedstonerProZ
//         */
//        scoped {
//            AudioAttributes.Builder()
//                .setContentType(AUDIO_CONTENT_TYPE_MUSIC)
//                .setUsage(USAGE_MEDIA)
//                .build()
//        }
//
//        /**
//         * [AudioAttributes] instance injected by koin
//         * @author MaskedRedstonerProZ
//         */
//        val audioAttributes: AudioAttributes by inject(AudioAttributes::class.java, scopeQualifier)

        /**
         * [ExoPlayer] dependency definition for injection
         * @author MaskedRedstonerProZ
         */
        scoped {
            ExoPlayer.Builder(androidContext()).build().apply {
                setAudioAttributes(AudioAttributes.Builder()
                    .setContentType(AUDIO_CONTENT_TYPE_MUSIC)
                    .setUsage(USAGE_MEDIA)
                    .build(), true)
                setHandleAudioBecomingNoisy(true)
                repeatMode = Player.REPEAT_MODE_ALL
            }
        }

        /**
         * [DefaultDataSource.Factory] dependency definition for injection
         * @author MaskedRedstonerProZ
         */
        scoped {
            DefaultDataSource.Factory(androidContext())
        }

        /**
         * [LocalMusicSource] dependency definition for injection
         * @author MaskedRedstonerProZ
         */
        scoped { LocalMusicSource(db.dao) }
    }
}