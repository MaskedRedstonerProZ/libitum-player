package tk.maskedredstonerproz.libitumplayer.feature_music.presentation.util

import tk.maskedredstonerproz.libitumplayer.feature_music.domain.exoplayer.service.MusicServiceConnection

/**
 * The [MusicServiceConnection] status
 * @author MaskedRedstonerProZ
 */
enum class Status {

    /**
     * The successful [Status] entry
     * @author MaskedRedstonerProZ
     */
    SUCCESS,

    /**
     * The unsuccessful [Status] entry
     * @author MaskedRedstonerProZ
     */
    ERROR,

    /**
     * The loading in progress [Status] entry
     * @author MaskedRedstonerProZ
     */
    LOADING
}