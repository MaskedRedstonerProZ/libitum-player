package tk.maskedredstonerproz.libitumplayer.feature_music.presentation.util

/**
 * The app's constants
 * @author MaskedRedstonerProZ
 */
object Constants {

    /**
     * The debug log tag constant
     * @author MaskedRedstonerProZ
     */
    const val TAG = "Libitum Player"

    /**
     * The database name constant
     * @author MaskedRedstonerProZ
     */
    const val DB_NAME = "songs"

    /**
     * The exoplayer media root id
     * @author MaskedRedstonerProZ
     */
    const val MEDIA_ROOT_ID = "root"

    /**
     * The tag for the music service
     * @author MaskedRedstonerProZ
     */
    const val SERVICE_TAG = "music-service"

    /**
     * The permission checking request code constant
     * @author MaskedRedstonerProZ
     */
    const val PERMISSION_CHECK_REQUEST_CODE = 75456

    /**
     * The notification channel id constant
     * @author MaskedRedstonerProZ
     */
    const val NOTIFICATION_CHANNEL_ID = "music"

    /**
     * The notification id constant
     * @author MaskedRedstonerProZ
     */
    const val NOTIFICATION_ID = 1

    /**
     * The error constant
     * @author MaskedRedstonerProZ
     */
    const val ERROR = "error"
}