package tk.maskedredstonerproz.libitumplayer.feature_music.domain.use_case

import tk.maskedredstonerproz.libitumplayer.feature_music.data.db.SongDao
import tk.maskedredstonerproz.libitumplayer.feature_music.domain.model.InvalidSongException
import tk.maskedredstonerproz.libitumplayer.feature_music.domain.model.Song

/**
 * The [AddSong] use case that manages adding songs to the database
 * @param dao The [SongDao] implementation for accessing the database
 * @author MaskedRedstonerProZ
 */
class AddSong(
    private val dao: SongDao
) {

    /**
     * Adds a [Song] to the database
     * @param song The [Song] to remove
     * @author MaskedRedstonerProZ
     */
    @Throws(InvalidSongException::class)
    suspend operator fun invoke(song: Song) {

        if (song.storagePath.isBlank()) {
            throw InvalidSongException("The file path can't be blank.")
        }

        if (song.name.isBlank()) {
            throw InvalidSongException("The song name can't be blank.")
        }

        if (song.artist.isBlank()) {
            throw InvalidSongException("The song author name can't be blank.")
        }

        dao.insertSong(song)
    }

}