package tk.maskedredstonerproz.libitumplayer.feature_music.framework.exoplayer

import android.support.v4.media.MediaMetadataCompat
import tk.maskedredstonerproz.libitumplayer.feature_music.domain.model.Song

fun MediaMetadataCompat.toSong(): Song? {
    return description?.let {
        val id = it.mediaId.toString().split('_')[0]
        Song(
            id = id.toInt(),
            name = it.title.toString(),
            artist = it.subtitle.toString(),
            storagePath = it.mediaUri?.toString()?: ""
        )
    }
}