package tk.maskedredstonerproz.libitumplayer.ui.theme

import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.runtime.Composable

/**
 * My custom colour palette
 * @author MaskedRedstonerProZ
 */
private val DarkColorPalette = darkColors(
    primary = RedAccent,
    background = DarkGray,
    onBackground = TextWhite,
    onPrimary = TextWhite,
    surface = LightGray,
    onSurface = RedAccent,
    onSecondary = BlueAccent
)

/**
 * Theme composable
 * @param content Composables that this theme is applied on
 */
@Composable
fun LibitumPlayerTheme(
    content: @Composable () -> Unit
) {
    // Apply all the properties to the MaterialTheme composable
    MaterialTheme(
        colors = DarkColorPalette,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}