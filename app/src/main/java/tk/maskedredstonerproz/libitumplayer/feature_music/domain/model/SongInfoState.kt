package tk.maskedredstonerproz.libitumplayer.feature_music.domain.model

import tk.maskedredstonerproz.libitumplayer.R
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.util.UiText

/**
 * State for keeping track of song import info
 * @param importedFileName The imported file name(Initially the hint
 * as to how to obtain such a name)
 * @param songName The name of the [Song]
 * @param songArtistName The name of the [Song]'s author
 * @author MaskedRedstonerProZ
 */
data class SongInfoState(
    val importedFileName: UiText = UiText.StringResource(R.string.file_name_hint),
    val songName: String = "",
    val songArtistName: String = ""
)