package tk.maskedredstonerproz.libitumplayer.feature_music.domain.exoplayer.service

import android.content.ComponentName
import android.content.Context
import android.os.Bundle
import android.support.v4.media.MediaBrowserCompat
import android.support.v4.media.MediaMetadataCompat
import android.support.v4.media.session.MediaControllerCompat
import android.support.v4.media.session.PlaybackStateCompat
import android.util.Log
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import tk.maskedredstonerproz.libitumplayer.feature_music.framework.exoplayer.service.MusicService
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.util.Constants.ERROR
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.util.Event.MusicServiceConnectionEvent
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.screens.main_screen.MainViewModel

/**
 * The connection between the [MainViewModel] and [MusicService] that allows
 * easy interaction between the two
 * @param context The application [Context] of the [MusicServiceConnection]
 * @author MaskedRedstonerProZ
 */
class MusicServiceConnection(
    context: Context
) {

    /**
     * The connection status
     * @author MaskedRedstonerProZ
     */
    private val _isConnected = MutableSharedFlow<MusicServiceConnectionEvent<Boolean>>()
    val isConnected = _isConnected.asSharedFlow()

    /**
     * The connection status error(if it exists that is)
     * @author MaskedRedstonerProZ
     */
    private val _error = MutableSharedFlow<MusicServiceConnectionEvent<Boolean>>()
    val error = _error.asSharedFlow()

    /**
     * The state of the music playback
     * @author MaskedRedstonerProZ
     */
    private val _playbackState = mutableStateOf<PlaybackStateCompat?>(null)
    val playbackState: State<PlaybackStateCompat?> = _playbackState

    /**
     * The state that contains the currently playing song
     * @author MaskedRedstonerProZ
     */
    private val _currentPlayingSong = mutableStateOf<MediaMetadataCompat?>(null)
    val currentPlayingSong: State<MediaMetadataCompat?> = _currentPlayingSong

    lateinit var mediaController: MediaControllerCompat

    private val mediaBrowserConnectionCallback = MediaBrowserConnectionCallback(context)

    private val mediaBrowser = MediaBrowserCompat(
        context,
        ComponentName(
            context,
            MusicService::class.java
        ),
        mediaBrowserConnectionCallback,
        null
    ).apply { connect() }

    val transportControls: MediaControllerCompat.TransportControls
        get() = mediaController.transportControls

    fun subscribe(parentId: String, callback: MediaBrowserCompat.SubscriptionCallback) {
        mediaBrowser.subscribe(parentId, callback)
    }

    fun unsubscribe(parentId: String, callback: MediaBrowserCompat.SubscriptionCallback) {
        mediaBrowser.unsubscribe(parentId, callback)
    }

    private inner class MediaBrowserConnectionCallback(
        private val context: Context
    ) : MediaBrowserCompat.ConnectionCallback() {

        override fun onConnected() {
            Log.d("MusicServiceConnection", "CONNECTED")
            mediaController = MediaControllerCompat(context, mediaBrowser.sessionToken).apply {
                registerCallback(MediaContollerCallback())
            }
            CoroutineScope(Dispatchers.Unconfined).launch {
                _isConnected.emit(
                    MusicServiceConnectionEvent.success(true)
                )
            }
        }

        override fun onConnectionSuspended() {
            Log.d("MusicServiceConnection", "SUSPENDED")

            CoroutineScope(Dispatchers.Unconfined).launch {
                _isConnected.emit(
                    MusicServiceConnectionEvent.error(
                        "The connection was suspended", false
                    )
                )
            }
        }

        override fun onConnectionFailed() {
            Log.d("MusicServiceConnection", "FAILED")
            CoroutineScope(Dispatchers.Unconfined).launch {
                _isConnected.emit(
                    MusicServiceConnectionEvent.error(
                        "Couldn't connect to media browser", false
                    )
                )
            }
        }
    }

    private inner class MediaContollerCallback : MediaControllerCompat.Callback() {

        override fun onPlaybackStateChanged(state: PlaybackStateCompat?) {
            _playbackState.value = state
        }

        override fun onMetadataChanged(metadata: MediaMetadataCompat?) {
            _currentPlayingSong.value = metadata
        }

        override fun onSessionEvent(event: String?, extras: Bundle?) {
            super.onSessionEvent(event, extras)
            CoroutineScope(Dispatchers.Unconfined).launch {
                when (event) {
                    ERROR -> _error.emit(
                        MusicServiceConnectionEvent.error(
                            "Couldn't connect to the server. Please check your internet connection."
                        )
                    )
                }
            }
        }

        override fun onSessionDestroyed() {
            mediaBrowserConnectionCallback.onConnectionSuspended()
        }
    }
}