package tk.maskedredstonerproz.libitumplayer.feature_music.domain.use_case

import tk.maskedredstonerproz.libitumplayer.feature_music.data.db.SongDao
import tk.maskedredstonerproz.libitumplayer.feature_music.domain.model.Song

/**
 * The [DeleteSong] use case that manages removing songs from the database
 * @param dao The [SongDao] implementation for accessing the database
 * @author MaskedRedstonerProZ
 */
class DeleteSong(
    private val dao: SongDao
) {

    /**
     * Removes a [Song] from the database
     * @param song The [Song] to remove
     * @author MaskedRedstonerProZ
     */
    suspend operator fun invoke(song: Song) {
        dao.deleteSong(song)
    }

}