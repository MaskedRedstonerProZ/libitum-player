package tk.maskedredstonerproz.libitumplayer.ui.theme

import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Shapes
import androidx.compose.ui.unit.dp

/**
 * Shapes of composables
 * @author MaskedRedstonerProZ
 */
val Shapes = Shapes(
    small = RoundedCornerShape(topStart = 0.dp, topEnd = 10.dp, bottomStart = 0.dp, bottomEnd = 10.dp),
    medium = RoundedCornerShape(5.dp),
    large = RoundedCornerShape(10.dp)
)