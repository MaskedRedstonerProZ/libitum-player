package tk.maskedredstonerproz.libitumplayer.feature_music.presentation.screens.song_import_screen

import android.net.Uri
import android.os.Build
import android.os.Environment
import android.util.Log
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.DrawerState
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.CoroutineScope
import org.koin.java.KoinJavaComponent.inject
import tk.maskedredstonerproz.libitumplayer.R
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.components.StandardAppBar
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.components.StandardTextField
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.util.Event.*
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.util.Screen.*
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.util.TestTag.*
import tk.maskedredstonerproz.libitumplayer.feature_music.domain.model.Song
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.util.Constants.TAG
import java.io.File

/**
 * Screen composable for the [Song] importing screen
 * @author MaskedRedstonerProZ
 */
@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun SongImportScreen(
    scope: CoroutineScope,
    drawerState: DrawerState
) {

    /**
     * [SongImportViewModel] instance injected by koin
     * @author MaskedRedstonerProZ
     */
    val songImportViewModel: SongImportViewModel by inject(SongImportViewModel::class.java)

    /**
     * Root column composable containing the rest of the column ui
     * @author MaskedRedstonerProZ
     */
    Column(
        Modifier
            .fillMaxSize()
            .testTag(SongImportScreenTestTag())
            .background(MaterialTheme.colors.background)
    ) {
        /**
         * The standard app bar on the top of every screen, with all it's params
         * @author MaskedRedstonerProZ
         */
        StandardAppBar(
            screen = SongImportScreen,
            modifier = Modifier.testTag(AppBarTestTag()),
            scope = scope,
            drawerState = drawerState
        )

        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.SpaceEvenly,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            val context = LocalContext.current

            val picker = rememberLauncherForActivityResult(ActivityResultContracts.GetMultipleContents()) {
                it.forEachIndexed { index, uri ->
                    Log.d(TAG, Uri.parse(uri.path.toString()).toString())
                    songImportViewModel.addToImportedSongsState(
                        index = index,
                        value =  File(Uri.parse(uri.lastPathSegment.toString().removePrefix("primary:")).toString())
                    )
                }

                songImportViewModel.onEvent(UpdateImportedFileName(false))
            }

            // file picker
            Row(
                Modifier
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    text = songImportViewModel.songInfoState.importedFileName.asString(),
                    modifier = Modifier
                        .background(
                            color = Color.Black,
                            shape = MaterialTheme.shapes.medium
                        )
                        .padding(10.dp),
                )
                Spacer(
                    Modifier.width(10.dp)
                )
                Button(
                    shape = MaterialTheme.shapes.medium,
                    modifier = Modifier
                        .testTag(ImportButtonTestTag())
                        .background(
                            color = Color.Black,
                            shape = MaterialTheme.shapes.medium
                        )
                        .padding(
                            start = 5.dp,
                            end = 5.dp
                        ),
                    onClick = {
                        songImportViewModel.onEvent(ResetCounter)
                        songImportViewModel.importedSongsState.removeRange(0, songImportViewModel.importedSongsState.size)
                        picker.launch("audio/*")
                    }
                ) {
                    Text(text = stringResource(id = R.string.btn_import))
                }
            }

            // Song name text field
            Row(
                Modifier
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically
            ) {
                StandardTextField(
                    value = songImportViewModel.songInfoState.songName,
                    modifier = Modifier
                        .background(
                            color = Color.Black,
                            shape = MaterialTheme.shapes.medium
                        )
                        .padding(5.dp),
                    hint = stringResource(id = R.string.song_name_hint),
                    onValueChange = {
                        songImportViewModel.onEvent(UpdateSongName(it))
                    }
                )
            }

            // Song author text field
            Row(
                Modifier
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically
            ) {
                StandardTextField(
                    value = songImportViewModel.songInfoState.songArtistName,
                    modifier = Modifier
                        .background(
                            color = Color.Black,
                            shape = MaterialTheme.shapes.medium
                        )
                        .padding(5.dp),
                    hint = stringResource(id = R.string.song_author_name_hint),
                    onValueChange = {
                        songImportViewModel.onEvent(UpdateSongArtistName(it))
                    }
                )
            }

            // Save info button
            Row(
                Modifier
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Button(
                    onClick = {

                        songImportViewModel.apply {

                            addSong("file://${Environment.getExternalStorageDirectory().path}/${importedSongsState[songImportViewModel.counter].path}")

                            if(counter == importedSongsState.size) {
                                onEvent(UpdateImportedFileName(true))
                                onEvent(ResetCounter)
//                                onEvent(UpdateSongName(""))
//                                onEvent(UpdateSongArtistName(""))
                                return@apply
                            }

                            onEvent(UpdateSongName(""))
                            onEvent(UpdateSongArtistName(""))

                            onEvent(IncreaseCounter)
                            onEvent(UpdateImportedFileName(false))
                        }
                    },
                    shape = MaterialTheme.shapes.medium,
                    modifier = Modifier
                        .background(
                            color = Color.Black,
                            shape = MaterialTheme.shapes.medium
                        )
                        .padding(
                            start = 5.dp,
                            end = 5.dp
                        )
                ) {
                    Text(text = stringResource(id = R.string.btn_save))
                }
            }
        }
    }
}