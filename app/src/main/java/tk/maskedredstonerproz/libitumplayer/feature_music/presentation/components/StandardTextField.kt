package tk.maskedredstonerproz.libitumplayer.feature_music.presentation.components

import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

/**
 * Standard Text field for this app
 * @param value The input text to be shown in the text field
 * @param onValueChange The callback that is triggered when the input service updates the text.
 * An updated text comes as a parameter of the callback
 * @param modifier A [Modifier] for this text field
 * @author MaskedRedstonerProZ
 */
@Composable
fun StandardTextField(
    value: String,
    onValueChange: (it: String) -> Unit,
    modifier: Modifier,
    hint: String
) {
    OutlinedTextField(
        value = value,
        singleLine = true,
        shape = MaterialTheme.shapes.medium,
        modifier = modifier,
        placeholder = {
            Text(text = "$hint:")
        },
        textStyle = MaterialTheme.typography.body1,
        onValueChange = {
            onValueChange(it)
        }
    )
}