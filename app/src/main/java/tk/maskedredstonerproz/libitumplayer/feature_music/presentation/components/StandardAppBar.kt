package tk.maskedredstonerproz.libitumplayer.feature_music.presentation.components

import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import tk.maskedredstonerproz.libitumplayer.R
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.util.Screen
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.util.TestTag.*

/**
 * The standard app bar present on every screen
 * @author MaskedRedstonerProZ
 * @param modifier The [Modifier] to be applied to this StandardAppBar
 * @param screen The screen the app bar is composed on
 * @param scope The coroutine scope we need to open/close the drawer
 * @param drawerState The state for opening/closing the drawer(via the coroutine scope)
 * @param playOptions The play options
 */
@Composable
fun StandardAppBar(
    modifier: Modifier,
    screen: Screen,
    scope: CoroutineScope,
    drawerState: DrawerState,
    playOptions: (@Composable () -> Unit)? = null
) {
    // Call the TopAppBar composable
    TopAppBar(
        // Set the title to <app_name>:<screen_name>, ex. Libitum Player: Home
        title = {
            Text(text = "${stringResource(id = R.string.app_name)}: ${screen.name}", modifier = Modifier.testTag(AppBarInfoTestTag()))
        },
        // Set the modifier
        modifier = modifier,
        // Set the navigation icon to an icon button
        navigationIcon = {
            IconButton(onClick = {

                /*
                 * On click of the aforementioned icon button, launch a coroutine
                 * using the scope parameter
                 */
                scope.launch {
                    // Open the drawer
                    drawerState.open()
                }
            }
            ) {
                // Set the icon for the icon button
                Icon(Icons.Default.Menu, "")
            }
        },
        // Set the background and content colours + the play options icons(if necessary)
        backgroundColor = Color.Black,
        contentColor = MaterialTheme.colors.onPrimary,
        actions = {
            if (playOptions == null) {
                return@TopAppBar
            }

            playOptions()
        }
    )
}