package tk.maskedredstonerproz.libitumplayer

import android.app.Application
import android.app.Instrumentation
import android.content.Context
import androidx.test.runner.AndroidJUnitRunner

/**
 * Android test runner
 * @author MaskedRedstonerProZ
 */
class TestRunner: AndroidJUnitRunner() {

    /**
     * Test application initialization
     * @author MaskedRedstonerProZ
     */
    override fun newApplication(
        cl: ClassLoader?,
        className: String?,
        context: Context?
    ): Application {

        return Instrumentation.newApplication(
            LibitumPlayerTest::class.java,
            context
        )

    }
}