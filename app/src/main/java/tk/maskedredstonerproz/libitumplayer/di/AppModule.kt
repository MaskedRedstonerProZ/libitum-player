package tk.maskedredstonerproz.libitumplayer.di

import androidx.room.Room
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import org.koin.java.KoinJavaComponent.inject
import tk.maskedredstonerproz.libitumplayer.feature_music.data.db.SongDatabase
import tk.maskedredstonerproz.libitumplayer.feature_music.domain.exoplayer.service.MusicServiceConnection
import tk.maskedredstonerproz.libitumplayer.feature_music.domain.use_case.*
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.screens.main_screen.MainViewModel
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.screens.song_import_screen.SongImportViewModel
import tk.maskedredstonerproz.libitumplayer.feature_music.presentation.util.Constants

/**
 * Application module for injecting dependencies
 * @author MaskedRedstonerProZ
 */
val appModule = module {

    /**
     * [SongUseCases] instance injected by koin
     * @author MaskedRedstonerProZ
     */
    val songUseCases: SongUseCases by inject(SongUseCases::class.java)

    /**
     * [MusicServiceConnection] instance injected by koin
     * @author MaskedRedstonerProZ
     */
    val musicServiceConnection: MusicServiceConnection by inject(MusicServiceConnection::class.java)

    /**
     * [MainViewModel] dependency definition for injection
     * @author MaskedRedstonerProZ
     */
    single {
        MainViewModel(songUseCases = songUseCases, musicServiceConnection = musicServiceConnection)
    }

    /**
     * [SongImportViewModel] dependency definition for injection
     * @author MaskedRedstonerProZ
     */
    single {
        SongImportViewModel(songUseCases)
    }

    /**
     * [SongDatabase] dependency definition for injection
     * @author MaskedRedstonerProZ
     */
    single {
        Room.databaseBuilder(
            androidContext(),
            SongDatabase::class.java,
            Constants.DB_NAME
        ).build()
    }

    /**
     * [SongDatabase] instance injected by koin
     * @author MaskedRedstonerProZ
     */
    val db: SongDatabase by inject(SongDatabase::class.java)

    /**
     * [SongUseCases] dependency definition for injection
     * @author MaskedRedstonerProZ
     */
    single {
        SongUseCases(
            deleteSong = DeleteSong(db.dao),
            addSong = AddSong(db.dao),
        )
    }

    /**
     * [MusicServiceConnection] dependency definition for injection
     * @author MaskedRedstonerProZ
     */
    single {
        MusicServiceConnection(
            androidContext()
        )
    }
}